# edyst_assignment
implement a job queue feature using flask web app


#### Database Initialization (locally)

Run below command to initialize database
```bash
flask db init
flask db migrate
flask db upgrade
```

### Running locally

Run following commend to run flask app 
```bash
install and activate virtual environment
cd edyst_assignment
pip install -r requirements/dev.txt
install redis on local machine
npm install
flask start  # run the webpack dev server and flask server using concurrently

```
Run following command to run worker for async process
```
activate virtualenv
python workers.py
```

Enter http://localhost:5000 URL in browser. You woll see registration screen.

# Flow to test application
1. On home click on register
2. Enter details and submit form
3. login using credentials submited before

 

## Running Tests
To run all tests, run

```bash
flask test 
```

