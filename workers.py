from edyst_assignment.app import create_app
from edyst_assignment.extensions import rq
app = create_app()


with app.app_context():
    default_worker = rq.get_worker()
    default_worker.work()