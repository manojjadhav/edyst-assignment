# -*- coding: utf-8 -*-
"""Create an application instance."""
from edyst_assignment.app import create_app

app = create_app()
