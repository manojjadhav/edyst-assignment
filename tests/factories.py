# -*- coding: utf-8 -*-
"""Factories to help in tests."""
from factory import PostGenerationMethodCall, Sequence
from factory.alchemy import SQLAlchemyModelFactory

from edyst_assignment.database import db
from edyst_assignment.user.models import User
from edyst_assignment.words_count.models import WordCount


class BaseFactory(SQLAlchemyModelFactory):
    """Base factory."""

    class Meta:
        """Factory configuration."""

        abstract = True
        sqlalchemy_session = db.session


class UserFactory(BaseFactory):
    """User factory."""

    username = Sequence(lambda n: f"user{n}")
    email = Sequence(lambda n: f"user{n}@edyst.com")
    password = PostGenerationMethodCall("set_password", "edyst")
    active = True

    class Meta:
        """Factory configuration."""

        model = User


class WordCountFactory(BaseFactory):
    """ word count Factory"""

    url = Sequence(lambda n: f"https://google.com/{n}")
    count = Sequence(lambda n: n)

    class Meta:
        """Factory configuration."""

        model = WordCount