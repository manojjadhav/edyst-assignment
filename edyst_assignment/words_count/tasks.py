import requests
from .models import WordCount
from edyst_assignment.extensions import rq

@rq.job
def count_words_for_url(url):
    """ count word for given url"""
    resp = requests.get(url)

    count = len(resp.text.split())
    word_obj = WordCount(url=url, count=count)
    word_obj.save()
    return word_obj



