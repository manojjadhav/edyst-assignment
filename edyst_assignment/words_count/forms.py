# -*- coding: utf-8 -*-
"""User forms."""
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import URL


class URLForm(FlaskForm):
    """Register form."""

    url = StringField(
        "url", validators=[URL(), ]
    )

    def __init__(self, *args, **kwargs):
        """Create instance."""
        super(URLForm, self).__init__(*args, **kwargs)
        self.user = None

