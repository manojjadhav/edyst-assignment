""" word count view"""
from flask_login import login_required
from .forms import URLForm
from flask import (
    Blueprint,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from edyst_assignment.utils import flash_errors
from .models import WordCount
from .tasks import count_words_for_url

blueprint = Blueprint("word", __name__, url_prefix="/word_count", static_folder="../static")


@blueprint.route("/", methods=["GET", "POST"])
@login_required
def word_count():
    """Word count ."""
    form = URLForm(request.form)
    current_app.logger.info("Hello from the home page!")
    if form.validate_on_submit():
        current_app.logger.info(f"Hello from the home page! {form.url.data}")
        # TODO: start job in queue
        count_words_for_url.queue(form.url.data)
        flash("You are request is Queued. ", "success")
        return redirect(url_for("word.word_count1"))
    else:
        flash_errors(form)

    return render_template("word_count/word_count_submit.html", form=form)


@blueprint.route("/results", methods=[ "GET"])
@login_required
def word_count_results():
    """Word count"""
    form = URLForm(request.form)
    response = WordCount.query.all()

    return render_template("word_count/word_count_display.html", form=response)