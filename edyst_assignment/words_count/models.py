from edyst_assignment.database import (
    Column,
    PkModel,
    db,
    reference_col,
    relationship,
)


class WordCount(PkModel):
    """Store word count for given url."""

    __tablename__ = "word_cnt"
    url = Column(db.String(80), unique=True, nullable=False)
    user_id = reference_col("users", nullable=True)
    user = relationship("User", backref="wordcount")

    count = Column(db.Integer, nullable=True)
